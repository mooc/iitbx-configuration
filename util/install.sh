#!/bin/sh

sudo apt-get install -y build-essential software-properties-common python-software-properties curl git-core libxml2-dev libxslt1-dev python-pip python-apt python-dev sshpass
sudo pip install --upgrade pip
sudo pip install --upgrade virtualenv
cd /var/tmp
git clone http://10.129.50.2/mooc/iitbx-configuration.git configuration
cd /var/tmp/configuration
sudo pip install -r requirements.txt
cd /var/tmp/configuration/playbooks && sudo ansible-playbook -c local ./edx_sandbox.yml -i "localhost,"